package me.connect.sdk.java.samplekt.db

enum class ActionStatus {
    PENDING,
    HISTORIZED
}