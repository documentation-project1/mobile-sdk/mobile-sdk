package me.connect.sdk.java.samplekt

object Constants {
    const val WALLET_NAME = "wallet-name"
    const val PREFS_NAME = "prefs"
    const val SPONSEE_ID = "sponsee_id"
    const val PROVISION_TOKEN = "provision_token"
    const val FCM_TOKEN = "fcm_token"
    const val FCM_TOKEN_SENT = "fcm_token_sent"
    const val PROVISION_TOKEN_RETRIEVED = "provision_token_retrieved"
    const val PLACEHOLDER_SERVER_URL = "placeholder_server_url"
    //URL of your server as a sponsor that would return signed provision token
    const val SERVER_URL = "placeholder_server_url" //enter here your URL instead of placeholder
}
