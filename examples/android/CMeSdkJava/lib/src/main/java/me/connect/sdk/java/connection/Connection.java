package me.connect.sdk.java.connection;

public interface Connection {
    String getConnectionType();
}
