package me.connect.sdk.java;

public class AgencyConfig {
    private AgencyConfig() {
    }

    public static final String DEFAULT = "{\"agency_url\":\"http://agency.evernym.com\",\"agency_did\":\"DwXzE7GdE5DNfsrRXJChSD\",\"agency_verkey\":\"844sJfb2snyeEugKvpY7Y4jZJk9LT6BnS6bnuKoiqbip\",\"agent_seed\":null,\"enterprise_seed\":null}";
}
