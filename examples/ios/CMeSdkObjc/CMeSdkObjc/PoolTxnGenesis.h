//
//  PoolTxnGenesis.h
//  CMeSdkObjc
//
//  Created by Norman Jarvis on 5/30/19.
//  Copyright © 2019 Evernym Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString * const poolTxnGenesis;

@interface PoolTxnGenesis : NSObject

@end

NS_ASSUME_NONNULL_END
