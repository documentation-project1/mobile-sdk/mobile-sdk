//
//  StagingPoolTxnGenesis.h
//  CMeSdkObjc
//
//  Created by Predrag Jevtic on 16/09/2020.
//  Copyright © 2020 Evernym Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef StagingPoolTxnGenesis_h
#define StagingPoolTxnGenesis_h

extern NSString * const stagingPoolTxnGenesisDef;

#endif
