//
//  DevTeam1PoolTxnGenesis.h
//  CMeSdkObjc
//
//  Created by evernym on 17/03/21.
//  Copyright © 2021 Norman Jarvis. All rights reserved.
//

#ifndef DevTeam1PoolTxnGenesis_h
#define DevTeam1PoolTxnGenesis_h

extern NSString * const devTeam1PoolTxnGenesisDef;

#endif /* DevTeam1PoolTxnGenesis_h */
