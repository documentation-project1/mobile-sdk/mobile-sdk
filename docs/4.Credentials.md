# 4. Credentials

The high level overview of Credential issuance steps:

1. Download and Parse received Credential Offer message
2. Create Credential state object using parsed Credential Offer message
    1. Serialize Credential state object and save serialized representation
    1. Update message (connected to Credential Offer) status on the Agent as reviewed
3. Accept Credential Offer
    1. Deserialize associated Connection state object
    1. Deserialize Credential state object
    1. Send Credential Request message
    1. Await Credential status is completed
    1. Serialize Credential state object and save serialized representation
4. Reject Credential Offer
    1. Deserialize associated Connection state object
    1. Deserialize Credential state object
    1. Send Credential Reject message
    1. Serialize Credential state object and save serialized representation

> **NOTE:** library should be initialized before using credentials API. See [initialization documentation](2.Initialization.md)

## Accept Credentials

### 1. Get Credential Offer from pending messages

1. Download pending messages (see [messages documentation](MessagesFlow.md) for messages downloading information). Pending messages with `credential-offer` type should be used.  Extract credential offer JSON string from the downloaded message (value of `decryptedPayload` field).
    
    #### Example of Credential Offer
    
    ```json
    [
        {
            "claim_id": "123",
            "claim_name": "DEMO-Transcript",
            "cred_def_id": "R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1",
            "credential_attrs": {
                "DEMO-College Name": "Faber College",
                "DEMO-Degree": "Computer Science",
                "DEMO-GPA": "4.0",
                "DEMO-Major": "SSI Software Engineering",
                "DEMO-Student Name": "Alice Andersen"
            },
            "from_did": "Byyf6DiChBaukbgPssuFi1",
            "libindy_offer": "{\"schema_id\":\"R9kuPrDFDVKNRL1oFpRxg2:2:DEMO-Transcript:1.0\",\"cred_def_id\":\"R9kuPrDFDVKNRL1oFpRxg2:3:CL:33627:tag1\",\"key_correctness_proof\":{\"c\":\"18409263165024561057868297548338734429203182181892601294150224288240356148969\",\"xz_cap\":\"89092862071754482768568121750605441740780661720589296980571662371912825324849589562417824532135192968565372473462126811235969943884283863879335732191646688609906727685365837864115018709597962744791616448487915667192875058592815636339507614499227407943494357843528056033773721711029552842160396617016884835556020495082061907517897139296921868726098793929456464992060745065258560960963184459787524683244617708108982997614880502648066019837184627185880652671440406751004823716138038578482286313872937436689907376336392198624082502390842170298696350711224912393805607280115907250438353352931044945179040370553648653866272291219097868230012644023837579478565817467758686729145484709030823195983554\",\"xr_cap\":[[\"demo-major\",\"164587753942899494168745746702534281563037669288218682692257391165325041732168189460545115812308404279240589114425046322356132181408031051286239643169009005958223435439799355211662444104531883082538176014292077920846413938760235906086381910034356130381286749206128094454452662283815044330730800323967902470312775790455241239653482745004223850680770052818976351902205471198454535294783397658320491100845383694178245356320509202610422971809318360852346231110471745351684915383461541209210464090117212587703482762934418809859227396852297417037033140072272676151762891624090259458948049903044932665550956351020903956472199708780575514718753305756346547634669337342447465198642113593266941556003489\"],[\"demo-gpa\",\"22199559154241071233540251324195337230927822968669546682924256492487853877953031345825385745900241025330375871523917849712725385790432951076987595680053019446054741259892564701293623250303595931063343966034351309995316956097733763166994791887902101149798874562034383975331980161354768409446422855147483195212437317537973845251280883872998417723398849872761881339298451486684674885318327796748378944659613162839511183514127487736387801234622230589841243519960874429783488747835807721882053729686382271314635805402653965411193898033208899024711454939860531264594452160431601266653407877024382554166965954564507888453238080543443825294612573362378277653648119231853727251371252426611250719223027\"],[\"demo-degree\",\"425307926357731961641889723351192532598285842402504657385351442966147456820187648192467747579412990962082780672922537000698541127067274541764366465881608778619655051450139613208362852093703153304455076974106861473420212168528190233224128135187035013850265971419193482519553936471029341985892219443902953021308950958867128132854325602761697845595230964616277064461362750199729352648568624273623621409353173348918863292813264771105478518129083486751236120470941869433608154015175461988708774026503367528484264960283828722023093081762732688330723751523175400425861380454412846687704806799430524033550455771282169951369159625112011956983708855437333914168956346464882619206522338259213757536326058\"],[\"demo-studentname\",\"332942112992078956065502211608387712655001083010169952511826075054945177324008673179434276989273295506738415570486847706494046829912089490672907344863578560956535931875873789276035124529730434622632506113551817581613699326755751216014910566217111708516008633400092766396982417384090165975121601372757060655383618934195784794468382484016906065941618773224768793098806073747798898613543519044035259337593748741589107633738885055731088913891966617458130135496565870795773171361810258169268160133602228280475389510872612504749862640093542170867364327914094068393807397625776548483684493102676731111623554447585743020640035792033063140954939015249185968692270629089072758373236129352580683143071761\"],[\"demo-collegename\",\"256956542286851333861178542347550843546602336608011961558790339355582543368619947132431033284018607829183717192553026370913923276337100278000711086078700912704406969815450902416202898668180355104154375118864951703873631679369560591960412167087463159579554031232611474483835459581596891709629200793913448526516843429369270281150318082896345684610971073438995144135178721242301138106737185350870630144213892199456597667151448810510989482504838867314779162444998243004059058257958923094769333676695275088360084008223593237541561581754838827142975493992967377714562756829715354497023402091089159902814924453530031297226196362145195501696869223672653009915485152284531305174191275718734671168638531\"],[\"master_secret\",\"411035279220097313141639496560361556200182820268236744181042170495644257271602560662636267278449740845231099678615487666867274088697078282727112089775108679200935406965394662679949604744630828792989232095978841363694220200918070829219562333021609323718741068752958915034422243090830005990644002315152710000100339840653484954546420674392862441143898339041785588558280237678400651047112646434338047479802903948283008613054301117081310752384053946023208896128041360508717835899081555400948972385606201372740711885971254389857009228358812537741320250162116765962040122767139702034734251663243075544887564157715654976094619015563754259692900247899890694680086596189292951372041405036180254469521378\"]]},\"nonce\":\"451547265861164114557369\"}",
            "msg_ref_id": "b060ca52-873e-4606-bf49-8ba83e4320a0",
            "msg_type": "CRED_OFFER",
            "schema_seq_no": 0,
            "thread_id": "467f6449-7d1f-4a9f-ada7-09d6444af083",
            "to_did": "Byyf6DiChBaukbgPssuFi1",
            "version": "0.1"
        }
    ]
    ```

The following fields could be used for user interaction:

* `claim_name` - name of the credential.
* `credential_attrs` - JSON object containing pairs of offered credential attributes and their values.

### 2. Create Credential state object using received Credential Offer

1. Create Credential state object

    ### iOS

    ```objc
    [appDelegate.sdkApi credentialCreateWithOffer: sourceId
            offer: message
            completion:^(NSError *error, NSInteger credentialHandle) {
                // ...
            }];
    ```

    ### Android
    ```java
    int credentialHandle = CredentialApi.credentialCreateWithOffer(sourceId, message).get();
    ```
    
    `sourceId` - any string\
    `message` - message downloaded on step 1.

1. Serialize Credential state object

    #### iOS
    ```objc
    [appDelegate.sdkApi credentialSerialize: credentialHandle
            completion:^(NSError *error, NSString *state) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    String serializedCredential = CredentialApi.credentialSerialize(credentialHandle).get();
    ```

1. Store serialized Credential for latter operations. \
   It is up to the developer regarding what data to store in the application. On of the possible formats may match the following structure:
   
    ```
    {
        "id" - string, // value of `thread_id` field. This can be useful to match messages receiving afterwards   
        "pwDid" - string, // point to connection 
        "serialized" - string, // serialized SDK object
        "name": string, // credential name
        "attributes": { // credential attributes
            <name>: <value>,
            ...
        }, // credential values
        "timestamp": int // optional, time of receiving a credential (it can be used for sorting) 
                         // it must be set when actual credential is received.
      }
   ```

1. Every time in the future you want to perform some operations using the created credential object you first need to fetch Credential object from the storage and next deserialize SDK object from its serialized representation (receive a new handle).

1. Update status of correspondent message on the Agent. See [messages documentation](MessagesFlow.md) for message update information.

### 3. Accept Credential Offer

If a user agreed to receive a credential related to the received offer, the following steps should be done in order to receive the actual credential.

1. Deserialize Connection state object associated with received Credential Offer

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionDeserialize:serializedConnection
            completion:^(NSError *error, NSInteger connectionHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int connectionHandle = ConnectionApi.connectionDeserialize(serializedConnection).get();
    ```

1. Deserialize Credential state object associated with accepted Credential Offer

    #### iOS
    ```objC
    [appDelegate.sdkApi credentialDeserialize:serializedCredential
            completion:^(NSError *error, NSInteger credentialHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int credOfferHandle = CredentialApi.credentialDeserialize(serializedCredOffer).get();
    ```

1. Send `Credential Request` message to the issuer

    #### iOS
    ```objc
    [appDelegate.sdkApi credentialSendRequest:credentialHandle
            connectionHandle:connectionHandle
            paymentHandle:0
            completion:^(NSError *error) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    CredentialApi.credentialSendRequest(credOfferHandle, connectionHandle, 0).get()
    ```

1. Await Credential received.

   When `Credential` message will be received from the Issuer the state of the Credential object will be equal `4` (`Accepted`).

   Call following code in a loop until the returned state is not equal `4` (`Accepted`).

    ### iOS

    ```objc
    while(1) {
        [appDelegate.sdkApi credentialGetState:credentialHandle            
                completion:^(NSError *error, NSInteger state)) {
                    if (state == 4){
                        break;
                    }
                }];
    }
    ```

    ### Android

    ```java
    int state = -1
    while (state != 4){
        state = CredentialApi.credentialUpdateState(handle).get();
    }
    ```

1. Serialize Credential state object

    #### iOS
    ```objC
    [appDelegate.sdkApi credentialSerialize:credentialHandle
            completion:^(NSError *error, NSString *state)) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    String serializedCredential = CredentialApi.credentialSerialize(credentialHandle).get();
    ```

1. Update a related record in the storage with the latest value of the serialized Credential state object and receiving timestamp.

### 4. Reject Credential Offer

If a user does not want to receive a credential related to the received offer, the following steps should be done in order to explicitly reject offer (notify Issuer).

1. Deserialize Connection state object associated with rejected Credential Offer

    #### iOS
    ```objC
    [appDelegate.sdkApi connectionDeserialize:serializedConnection
            completion:^(NSError *error, NSInteger connectionHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int connectionHandle = ConnectionApi.connectionDeserialize(serializedConnection).get();
    ```

1. Deserialize Credential state object associated with rejected Credential Offer

    #### iOS
    ```objC
    [appDelegate.sdkApi credentialDeserialize:serializedCredential
            completion:^(NSError *error, NSInteger credentialHandle) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    int credOfferHandle = CredentialApi.credentialDeserialize(serializedCredOffer).get();
    ```

1. Send `Credential Reject` message to the issuer

    #### iOS
    ```objc
    [appDelegate.sdkApi credentialReject:credentialHandle
            connectionHandle:connectionHandle
            comment:@"Rejection comment"
            completion:^(NSError *error) {
                // ...
            }];
    ```
    
    #### Android
    ```java
    CredentialApi.credentialReject(credOfferHandle, connectionHandle, "Rejection comment").get()
    ```

1. Serialize Credential state object

   #### iOS
    ```objC
    [appDelegate.sdkApi credentialSerialize:credentialHandle
            completion:^(NSError *error, NSString *state)) {
                // ...
            }];
    ```

   #### Android
    ```java
    String serializedCredential = CredentialApi.credentialSerialize(credentialHandle).get();
    ```

1. Update a related record in the storage with the latest value of the serialized Credential state object.

## Cache data for Proof generation - Optional

The first time SDK generates Proof using a specific credential it fetches the credential's public Credential Definition and Schema from the Ledger.
These operations take some time that increases the overall time taken to present proof to the remote side.
In order to decrease that time your application can fetch and cache these public entities right after receiving a credential.
Furthermore, it makes the Proof presentation independent of the Ledger connectivity as well.

### Steps

Assume you received Credential.

1. Assume that you just received and stored Credential, and the state of the Credential object is 4.
   
2. Fetch public Credential Definition and Schema from the Ledger associated with stored in the wallet credentials.

> **NOTE:** This function checks that data fetched and cached for all stored in the Wallet credentials

> **NOTE:** The recommended way is to call it in a separate thread as it may take several seconds.

   Java pseudocode:
    ```
        UtilsApi.vcxFetchPublicEntities()
    ```

   Objective-C pseudocode
    ```
        [[[ConnectMeVcx alloc] init] fetchPublicEntities...]

    ```

## Credential with attachments

Credentials with attachments can be issued and attachments can be shown in client application.

For example, attachments can be represented as base64 encoded strings, please refer to [Credentials with attachments example](CredentialsWithAttachments.md).

> **NOTE**: base64 encoded strings is just a representation of attachment, any other ways are valid and up to your system architecture.

## Next Step

Now your application is able to receive and store verifiable credentials.
You are ready to ready how to [fill proof requests](5.Proofs.md) using credentials in your wallet.
